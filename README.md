# ddrsync

## Description
The intent of this project is to provide bash scripts that emulate command of file copying, like `rsync` and `cp`, by making use of `dd` utility in order to be able to choose the block size to use during the copy operation. Actually, depending on the type of the file system, the choice of the block size can make the copy much more efficient than the usual file copy commands. For example, this is what we observed with a BeeGFS file system. By setting a relevant block size, the copy of a file with `dd` command can be 5 times faster than the `cp` command.

The content of this project is a rsync-like bash script command and a cp-like bash script command based on `dd` command. As an option, you can specify for each of them the block size you want to use when launching the command. 

It alse provides a file with a set of bash script functions that can be loaded and used interactively or within bash scripts.

### When to use or not to use

To be a relevant solution, those scritps can be used under some obvious conditions:
- the first one is that you have to operate file copy between 2 file systems that are sensitive to block size.
- the second one is that the size of the files to be copied is large enough to save time when copying.

In all cases, the classic commands rsync and cp are therefore preferable when copying deep trees populated by numerous small files and particularly in the absence of very large files. Actually, in such a case, bash scripts will take much more time to navigate the tree than conventional commands without any benefit of time saved in the absence of sufficiently large files.

### Limitations

The scripts implement an incomplete and basic behaviour of the corresponding commands.

The behaviour `ddrsync` is similar to `rsync -ah` except that it does not deal with symbolic links. A symbolic link si simply and purely ignored by `ddrsync`. For a directory `<dir>` in argument, `rsync` makes the difference between the expressions `<dir>` and `<dir>/`, the first one means the directory, the second one means the content of teh directory. With `ddrsync`, we givce at a directory (with or without trailing slash) only the last behaviour. Unlike `rscync`, `ddrsync` implements only one condition to trigger a synchronization of entry, which is at least one of those cases: destination entry does not exist, modify times differ, or size differ. 

The command `ddcp` is limited to the copy of a file. That means that the recursive copy of directory is not implemented.

## Installation

### Prerequisites

The prerequisite is to have both `dd` and `bc` (basic calculator) utilities pre-installed on the target host. 

Optionally, if you want to run tests used for continuous integration, you will have to install the Bash Automated Testing System (bats) framework.

### Download
Basically, to obtain ddrsynch utilities, just clone or download the code from this repository to put it somewhere on your machine, let say in `<ddrsync_install_dir>`. To make the commands available from your shell session, update your PATH environment variable accordingly: 

`$ export PATH=<ddrsync_install_dir>/bin:$PATH`

### Configuration

The configuration file is `ddrsync.conf` in `etc` directory in the installation root. It is minimalist and optional as it allows only to set the default value of the block size which is provided by the bash variable `DDRSYNC_BLOCK_SIZE`.

```
$ cat etc/ddrsync.conf
#
# Configuration of ddrsync
#

# Except if overloaded by environment set the block size suitable for
# standard silenus partition (10 chunks of 2M)
DDRSYNC_BLOCK_SIZE=${DDRSYNC_BLOCK_SIZE:-20M}
```
This example set de default value of block size to 20M but gives the **precedence** to the environment variable `DDRSYNC_BLOCK_SIZE` if set. That allows to manage different values depending on the environment you are in.

Not that the value given by the option `-b` overloads all the others.

## Usage

### Block size expression

The block size expression uses the one of `dd` syntax when setting `bs` (ex.: 512K, 10M, 2GB, etc., see `man dd`).

### Help
For each command you can obtain the usage with the `-h` option. 

**TIP**: At the end of the usage, you're informed about the current value of the block size that will be used. This is interesting to check it if you do not overload it explicitely by the option `-b`.

- For `ddcp`:

```
$ ddcp -h
Usages:
 ddcp -h
 ddcp [-v] [-b <block-size>] <src> <dst>
Description:
 Emulate cp by using dd utility to copy files with an adapted block size which
 is supposed to be more efficient than rsync or cp on some types of file system
 (ex.: BeeGFS).
Warning:
 Symbolic links are not taken into account and silently skipped if any (not implemented yet).
Arguments:
 <src>: the path to the source file.
 <dst>: the path to the destination file or directory.
Options:
 -h: help displaying this usage.
 -v: verbose mode.
 -b <block-size>: set the block-size to use for file copy. This option overload the value of
                  DDRSYNC_BLOCK_SIZE variable if defined. The block size expression <block-size>
                  must be a valid expression for dd (see 'man dd').
Files:
 <install_dir>/etc/ddrsync.conf if exists can be used to set the suitable DDRSYNC_BLOCK_SIZE.
Variables:
 DDRSYNC_BLOCK_SIZE: block size according to dd syntax. If not set, it is the dd default value (512)
 which is probably not the suitable value if you expect an efficient file copy.
 Current value is : 20M
```
- For `ddrsync`:
```
$ ddrsync -h
Usages:
 ddrsync -h
 ddrsync [-v] [-b <block-size>] <src> <dst>
Description:
 Emulate rsync (similar to 'rsync -ah' behaviour) by using dd utility to copy files
 with an adapted block size which is supposed to be more efficient than rsync or cp
 on some types of file system (ex.: BeeGFS).
Warning:
 This emulator is useful if you have big files. Don't use it if you have a deep directory
 tree with a lot of small files. In such a case, rsync command will be much more efficient.
 Symbolic links are not taken into account and silently skipped if any (not implemented yet).
 The synchronisation from a source file to a destination file is triggered if size files
 differs or modify times differs.
Arguments:
 <src>: the path to the source file or directory.
 <dst>: the path to the destination file or directory.
Options:
 -h: help displaying this usage.
 -v: verbose mode.
 -b <block-size>: set the block-size to use for file copy. This option overload the value of
                  DDRSYNC_BLOCK_SIZE variable if defined. The block size expression <block-size>
                  must be a valid expression for dd (see 'man dd').
Files:
 <install_dir>/etc/ddrsync.conf if exists can be used to set the suitable DDRSYNC_BLOCK_SIZE.
Variables:
 DDRSYNC_BLOCK_SIZE: block size according to dd syntax. If not set, it is the dd default value (512)
 which is probably not the suitable value if you expect an efficient file copy.
 Current value is : 20M
```

### ddrsync

For example, to synchronize the directory `src` to directory `dst` with the default value of block size :
```
$ ddrsync ./src ./dst

sent 128.84G bytes  received XXX bytes  1.25G bytes/sec
```
If you replay the command, nothing will be done:
```
$ ddrsync ./src ./dst

sent 0.00 bytes  received XXX bytes  0.00 bytes/sec
```
If you want to display wich entries are synchronized, use the verbose option (`-v`):
```
# First remove the destination directory to enforce the whole copy of source
$ rm -rf dst

# Set the verbose option
$ ddrsync -v ./src ./dst
- ./src/subdir/test_10G.img
- ./src/subdir
- ./src/test_100G.img
- ./src/test_10G.img
- ./src

sent 128.84G bytes  received XXX bytes  1.52G bytes/sec
```
Change the block size value for the copy of a file from the command line with option `-b`
```
# First, change the modify time of one file to enforce its synchronization
$ touch src/test_10G.img

# Set to 32M the block size
$ ddrsync -v -b 32M ./src ./dst
- ./src/test_10G.img

sent 10.73G bytes  received XXX bytes  1.25G bytes/sec
```

### ddcp

ddcp requires an existing regular file as first argument. The second one could be a path to a file (existent or not) or a directory. In the past case, the copy of the file will be done into the destination directory. If the destination file exists, it will we be overwritten.

If the copy is successfull, the exit code is 0, otherwise different of 0.

```
$ ddcp dst/test_10G.img src/

# Check the exit code
$ echo $?
0
```

Basically, this command calls `dd` so if you set the verbose option, the dd output will be displayed with relevant information about the transfer.
```
$ ddcp -v dst/test_10G.img src/
512+0 records in
512+0 records out
10737418240 bytes (11 GB, 10 GiB) copied, 6.3441 s, 1.7 GB/s
```

And finally, you can change the block size with the option `-b`:

```
$ ddcp -v -b 10M dst/test_10G.img src/
1024+0 records in
1024+0 records out
10737418240 bytes (11 GB, 10 GiB) copied, 9.95413 s, 1.1 GB/s
```

### Block size tuning

You can take advantage of the dd or ddcp command to test different values in order to find a pertinent block size according to your file system configurations. It is recommended to check that the obtained transfer rate is better than the one of the cp command.

- First, generate a file which is large enough (ex.: 10G): test_10G.img
```
$ dd if=/dev/random of=src/test_10G.img bs=1G count=10
10+0 records in
10+0 records out
10737418240 bytes (11 GB, 10 GiB) copied, 53.523 s, 201 MB/s
```
- Secondly, time the use of cp to make a copy of this file to get the reference value between your sources and destination targets.
```
$ /usr/bin/time -f "%e" cp src/test_10G.img dst/copy_10G.img
50.13
```
- Third, try different values of block size by timing ddcp the same way (put the output in a file)
```
# Test bs from 2M to 48M with a step of 2 (seq 2 2 48 = 2 4 6 8 10... 48)
$ for i in $(seq 2 2 10); do 
    t=$(/usr/bin/time -f "%e" ddcp -b ${i}M src/test_10G.img dst/copy_10G.img 2>&1); 
    echo "$i $t";
done >ddbench.log # the result is put in ddbench.log
```
- Fourth, sort by the times obtained to get the best values (the least)
```
$ sort -n -k2 ddbench.log
18 6.36
20 6.68
16 7.43
38 7.59
40 7.60
14 7.88
34 7.90
36 7.94
48 8.09
30 8.19
32 8.20
46 8.35
12 8.87
28 8.94
26 8.95
44 9.05
10 9.22
42 9.45
24 9.91
22 11.28
8 12.12
6 14.77
4 20.48
2 41.95
```
The first values of the lits are the more interesting (less time), so block size seems candidates are 18M or 20M. By comparing to the time of cp copy, we gain a lot by using one of those values. 

## Support
Feel free to open issue on this gitlab project.

## Contributing

## Authors and acknowledgment


## License
None

## Project status

