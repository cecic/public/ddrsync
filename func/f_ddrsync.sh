#
# Useful functions to manage files/directories copy
# 

# Default values
# DFT_DDRSYNC_BLOCK_SIZE="1M"

# Variable that is used to account the total amount of transfered data (in bytes)
DDRSYNC_DATA_TOTAL=0

# The block size to be used while copying files. This is the default except if overloaded by an exported vraiable
# DDRSYNC_BLOCK_SIZE=${DDRSYNC_BLOCK_SIZE:-${DFT_DDRSYNC_BLOCK_SIZE}}

# If not 0, more details will be printed, otherwise only errors and warning
DDRSYNC_VERBOSE=0

readonly DDRSYNC_ENTRY_REGULAR_FILE=1
readonly DDRSYNC_ENTRY_SYMLINK=2
readonly DDRSYNC_ENTRY_DIRECTORY=3
readonly DDRSYNC_ENTRY_OTHER=4

function ddrsync_init() {
  DDRSYNC_DATA_TOTAL=0
}

function ddrsync_block_size() {
  local block_size=$1

  [ -n "${block_size}" ] && { 

    # TODO: parse to check the block-size expression is ok
    DDRSYNC_BLOCK_SIZE=${block_size}
    return 0
  }

  echo ${DDRSYNC_BLOCK_SIZE}
}

function ddrsync_verbose() {
  local verbose=$1

  [ -n "${verbose}" ] && {

    # TODO: parse to check the verbose expression is ok
    DDRSYNC_VERBOSE=${verbose}
    return 0
  }

  echo ${DDRSYNC_VERBOSE}
}

function ddrsync_get_entry_type() {
  local entry=$1

  [ -n "${entry}" ] || return 1
  [ -e "${entry}" ] || return 2


  # Test first if symlink because if symlink points to a regular file/directory, the linked entry test will be true also
  [ -L "${entry}" ] && echo ${DDRSYNC_ENTRY_SYMLINK} && return 0;
  [ -f "${entry}" ] && echo ${DDRSYNC_ENTRY_REGULAR_FILE} && return 0;
  [ -d "${entry}" ] && echo ${DDRSYNC_ENTRY_DIRECTORY} && return 0;

  echo ${DDRSYNC_ENTRY_OTHER}
}


function ddrsync_cp_file() {
  local src=$1
  shift
  local dst=$1

  # [ -n "${src}" ] || return 1
  # [ -f "${src}" ] || return 2
  # On argument error, ddrsync_get_entry_type returns 1 or 2
  local sftype # local is considered as a command and return 0, so separate it from function 
               # call below to get the right return code of the function call
  sftype=$(ddrsync_get_entry_type ${src}) || return $?

  [ -n "${dst}" ] || return 3

  [ ${sftype} -eq ${DDRSYNC_ENTRY_REGULAR_FILE} ] || {
    echo "[ERROR] Not a regular file [${sftype}]" 1>&2
    return 4
  }

  # if dst is a directory, so we add the name of the file to sync to dst
  [ -d "${dst}" ] && dst=${dst}/$(basename ${src})

  local rcode=0
  local bs_exp
  [ -n "${DDRSYNC_BLOCK_SIZE}" ] && bs_exp="bs=${DDRSYNC_BLOCK_SIZE}"
  local tmp
  tmp=$(dd if=${src} of=${dst} ${bs_exp} 2>&1)
  rcode=$?

  [ $DDRSYNC_VERBOSE -gt 1 ] && echo "$tmp"

  return $rcode
}

function ddrsync_rsync_file() {
  local src=$1
  shift
  local dst=$1

  [ -n "${src}" ] || return 1
  [ -f "${src}" ] || return 2
  [ -n "${dst}" ] || return 3

  # if dst is a directory, so we add the name of the file to sync to dst
  [ -d "${dst}" ] && dst=${dst}/$(basename ${src})

  local tmp
  tmp=$(stat -c "%y|%s" ${src}) || return 5

  local sfdate=$(echo "$tmp" | cut -d'|' -f1)
  local sfstamp=$(date -d "${sfdate}" +%s%N)
  local sfsize=$(echo "$tmp" | cut -d'|' -f2)

  [ -f "${dst}" ] && {
    tmp=$(stat -c "%y|%s" ${dst}) || return 6

    local dfdate=$(echo "$tmp" | cut -d'|' -f1)
    local dfstamp=$(date -d "${dfdate}" +%s%N)
    local dfsize=$(echo "$tmp" | cut -d'|' -f2)

    # dst at least more recent and same size, nothing to do
    [ ${dfstamp} -eq ${sfstamp} -a ${sfsize} -eq ${dfsize} ] && return 0
  }

  # copy src to dst
  # echo "ddrsync_cp ${src} ${dst}"
  [ $DDRSYNC_VERBOSE -gt 0 ] && echo "- ${src}"
  ddrsync_cp_file "${src}" "${dst}" || return $?
  # change dst modify date by src one
  touch --date="${sfdate}" ${dst}
  DDRSYNC_DATA_TOTAL=$((sfsize + DDRSYNC_DATA_TOTAL))
}

function ddrsync_rsync_symlink() {
  local src=$1
  shift
  local dst=$1

  return 0
}

function ddrsync_rsync_directory() {
  local src=$1
  shift
  local dst=$1

  [ -n "${src}" ] || return 1
  [ -d "${src}" ] || return 2
  [ -n "${dst}" ] || return 3

  [ -e "${dst}" ] || mkdir ${dst} || return 4
  [ -d "${dst}" ] || return 4

  # Check if dates are same otherwise update the dst date with the src one
  # WARNING: dates include nanoseconds...
  local sddate
  local dddate
  sddate=$(stat -c "%y" ${src}) || return 5
  dddate=$(stat -c "%y" ${dst}) || return 6

  local sdstamp=$(date -d "${sddate}" +%s%N)
  local ddstamp=$(date -d "${dddate}" +%s%N)

  # Loop on all (thanks to '-a' option of ls command) src directory entries
  for e in $(ls -a ${src}); do
    # skip both . and .. relative directories to avoid infinite loop
    [ "${e}" = "." ] && continue
    [ "${e}" = ".." ] && continue

    ddrsync_rsync_selector "${src}/${e}" "${dst}/${e}"
  done

  # If necessary the date change must be done after having ended with directory content changes
  # to avoid that the modify date of current destination directory be changed due to content operations.
  [ ${ddstamp} -eq ${sdstamp} ] || {
    [ $DDRSYNC_VERBOSE -gt 0 ] && echo "- ${src}"
    touch --date="${sddate}" ${dst}
  }

}

function ddrsync_rsync_selector() {
  local src=$1
  shift
  local dst=$1

  # [ -n "${src}" ] || return 1
  local tmp
  tmp=$(ddrsync_get_entry_type ${src}) || return $?
  [ -n "${dst}" ] || return 3

  case ${tmp} in
    ${DDRSYNC_ENTRY_DIRECTORY}) ddrsync_rsync_directory ${src} ${dst} || return 6;;
    ${DDRSYNC_ENTRY_REGULAR_FILE}) ddrsync_rsync_file ${src} ${dst} || return 7;;
    ${DDRSYNC_ENTRY_SYMLINK}) ddrsync_rsync_symlink ${src} ${dst} || return 8;;
    *) echo "Unknown type [$tmp]" 1>&2; return 9;;
  esac
}

function ddrsync_human_amount() {
  local bytes=$1

  [ -n "${bytes}" ] || echo 0;

  # TODO: make the choice of unit between bytes and bibytes
  declare -r factor_byte="1000"
  declare -r factor_bibyte="1024"

  used_factor=${factor_byte}

  # take the decimal part
  local bytes_int
  bytes_int=$(printf "%0.0f\n" ${bytes}) || return 1
  amount_unit="G"
  upper_limit=1
  for count in $(seq 1 4); do
    amount_factor=${upper_limit}
    upper_limit=$((amount_factor*used_factor))
    [ ${bytes_int} -lt ${upper_limit} ] && {
      case ${count} in
        1) amount_unit="";;
        2) amount_unit="K";;
        3) amount_unit="M";;
        4) amount_unit="G";;
        5) return 1;;
      esac
      break
    }
  done

  human_amount=$(echo "scale=2; ${bytes} / ${amount_factor}" | bc -l)
  printf "%0.2f%s" ${human_amount} ${amount_unit}
}
