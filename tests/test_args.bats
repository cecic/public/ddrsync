
bats_require_minimum_version 1.5.0

# Load the tested functions and the default configuration
load ${BATS_TEST_DIRNAME}/../func/f_ddrsync.sh
load ${BATS_TEST_DIRNAME}/../etc/ddrsync.conf

#
# Global test setup
#

setup_file() {

  # Prepare the source directory
  export DDRSYNC_SRC_DIR=${BATS_SUITE_TMPDIR}/src
  mkdir ${DDRSYNC_SRC_DIR}
  echo "DATA" >${DDRSYNC_SRC_DIR}/existent-file
  echo "DATA" >${DDRSYNC_SRC_DIR}/dot-file
  # create a FIFO file for nonregular file tests
  mknod ${DDRSYNC_SRC_DIR}/nonregular-file p
  touch ${DDRSYNC_SRC_DIR}/empty-file
  mkdir ${DDRSYNC_SRC_DIR}/existent-dir

  # Create the destination directory
  export DDRSYNC_DST_DIR=${BATS_SUITE_TMPDIR}/dst
  mkdir ${DDRSYNC_DST_DIR}
  touch ${DDRSYNC_DST_DIR}/nondirectory-entry

  # echo "# BATS_SUITE_TMPDIR=${BATS_SUITE_TMPDIR}" >&3
}

# bats file_tags=arguments

#
# ddrsync_get_entry_type
#

# bats test_tags=ddrsync_get_entry_type:1

@test "ddrsync_get_entry_type argument test no argument (rcode 1)" {
    run -1 ddrsync_get_entry_type
}

# bats test_tags=ddrsync_get_entry_type:2

@test "ddrsync_get_entry_type argument test nonexistent source file (rcode 2)" {
    run -2 ddrsync_get_entry_type ${DDRSYNC_SRC_DIR}/nonexistent-file
}

#
# ddrsync_cp_file
#

# bats test_tags=ddrsync_cp_file:0

@test "ddrsync_cp_file argument test good arguments (rcode 0)" {
  run -0 ddrsync_cp_file ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_DST_DIR}/copied-file
}

# bats test_tags=ddrsync_cp_file:1

@test "ddrsync_cp_file argument test no argument (rcode 1)" {
    run -1 ddrsync_cp_file
}

# bats test_tags=ddrsync_cp_file:2

@test "ddrsync_cp_file argument test nonexistent source file (rcode 2)" {
    run -2 ddrsync_cp_file ${DDRSYNC_SRC_DIR}/nonexistent-file
}

# bats test_tags=ddrsync_cp_file:3

@test "ddrsync_cp_file argument test no destination file (rcode 3)" {
    run -3 ddrsync_cp_file ${DDRSYNC_SRC_DIR}/existent-file
}

# bats test_tags=ddrsync_cp_file:4
@test "ddrsync_cp_file argument test non regular source file (rcode 4)" {
  run -4 ddrsync_cp_file ${DDRSYNC_SRC_DIR}/nonregular-file ${DDRSYNC_DST_DIR}/copied-file
}


#
# ddrsync_rsync_file 
#

# bats test_tags=ddrsync_rsync_file:0

@test "ddrsync_rsync_file argument test good arguments (rcode 0)" {
  run -0 ddrsync_rsync_file ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_DST_DIR}/copied-file
}

# bats test_tags=ddrsync_rsync_file:1

@test "ddrsync_rsync_file argument test no argument (rcode 1)" {
    run -1 ddrsync_rsync_file
}

# bats test_tags=ddrsync_rsync_file:2

@test "ddrsync_rsync_file argument test nonexistent source file (rcode 2)" {
    run -2 ddrsync_rsync_file ${DDRSYNC_SRC_DIR}/non-existent-file
}

# bats test_tags=ddrsync_rsync_file:3

@test "ddrsync_rsync_file argument test no destination file (rcode 3)" {
    run -3 ddrsync_rsync_file ${DDRSYNC_SRC_DIR}/existent-file
}

# bats test_tags=ddrsync_rsync_file:2

@test "ddrsync_rsync_file argument test non regular source file (rcode 2)" {
  run -2 ddrsync_rsync_file ${DDRSYNC_SRC_DIR}/nonregular-file ${DDRSYNC_DST_DIR}/copied-file
}


#
# ddrsync_rsync_symlink
#

# bats test_tags=ddrsync_rsync_symlink:0

@test "ddrsync_rsync_symlink argument test good arguments (rcode 0)" {
  skip "ddrsync_rsync_symlink not implemented"
  run -0 ddrsync_rsync_symlink ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_DST_DIR}/copied-file
}

# bats test_tags=ddrsync_rsync_symlink:1

@test "ddrsync_rsync_symlink argument test no argument (rcode 1)" {
  skip "ddrsync_rsync_symlink not implemented"
  run -1 ddrsync_rsync_symlink
}

# bats test_tags=ddrsync_rsync_symlink:2

@test "ddrsync_rsync_symlink argument test nonexistent source file (rcode 2)" {
  skip "ddrsync_rsync_symlink not implemented"
  run -2 ddrsync_rsync_symlink ${DDRSYNC_SRC_DIR}/non-existent-file
}

# bats test_tags=ddrsync_rsync_symlink:3

@test "ddrsync_rsync_symlink argument test no destination file (rcode 3)" {
  skip "ddrsync_rsync_symlink not implemented"
  run -3 ddrsync_rsync_symlink ${DDRSYNC_SRC_DIR}/existent-file
}

# bats test_tags=ddrsync_rsync_symlink:4

@test "ddrsync_rsync_symlink argument test non regular source file (rcode 4)" {
  skip "ddrsync_rsync_symlink not implemented"
  run -4 ddrsync_rsync_symlink ${DDRSYNC_SRC_DIR}/nonregular-file ${DDRSYNC_DST_DIR}/copied-file
}

#
# ddrsync_rsync_directory
#

# bats test_tags=ddrsync_rsync_directory:0

@test "ddrsync_rsync_directory argument test good arguments (rcode 0)" {
  run -0 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/existent-dir ${DDRSYNC_DST_DIR}
}

# bats test_tags=ddrsync_rsync_directory:1

@test "ddrsync_rsync_directory argument test no argument (rcode 1)" {
    run -1 ddrsync_rsync_directory
}

# bats test_tags=ddrsync_rsync_directory:2

@test "ddrsync_rsync_directory argument test nonexistent source directory (rcode 2)" {
    run -2 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/nonexistent-dir {DDRSYNC_DST_DIR}
}

# bats test_tags=ddrsync_rsync_directory:3

@test "ddrsync_rsync_directory argument test no destination file (rcode 3)" {
    run -3 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/existent-dir
}

# bats test_tags=ddrsync_rsync_directory:4
@test "ddrsync_rsync_directory argument test non-existent destination (rcode 4)" {
  run -4 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/existent-dir ${DDRSYNC_DST_DIR}/nonexistent-dir/nonexistent-dir
}

# bats test_tags=ddrsync_rsync_directory:4
@test "ddrsync_rsync_directory argument test destination not a directory (rcode 4)" {
  run -4 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/existent-dir ${DDRSYNC_DST_DIR}/nondirectory-entry
}

#
# ddrsync_rsync_selector
#

# bats test_tags=ddrsync_rsync_selector:0

@test "ddrsync_rsync_selector argument test good arguments (rcode 0)" {
  run -0 ddrsync_rsync_selector ${DDRSYNC_SRC_DIR}/existent-dir ${DDRSYNC_DST_DIR}
}

# bats test_tags=ddrsync_rsync_selector:1

@test "ddrsync_rsync_selector argument test no argument (rcode 1)" {
    run -1 ddrsync_rsync_selector
}

# bats test_tags=ddrsync_rsync_selector:2

@test "ddrsync_rsync_selector argument test nonexistent source directory (rcode 2)" {
    run -2 ddrsync_rsync_selector ${DDRSYNC_SRC_DIR}/nonexistent-dir ${DDRSYNC_DST_DIR}
}

# bats test_tags=ddrsync_rsync_selector:2

@test "ddrsync_rsync_selector argument test nonexistent source file (rcode 2)" {
    run -2 ddrsync_rsync_selector ${DDRSYNC_SRC_DIR}/nonexistent-file ${DDRSYNC_DST_DIR}
}

# bats test_tags=ddrsync_rsync_selector:3

@test "ddrsync_rsync_selector argument test no destination directory (rcode 3)" {
    run -3 ddrsync_rsync_selector ${DDRSYNC_SRC_DIR}/existent-dir
}

# bats test_tags=ddrsync_rsync_selector:3

@test "ddrsync_rsync_selector argument test no destination file (rcode 3)" {
    run -3 ddrsync_rsync_selector ${DDRSYNC_SRC_DIR}/existent-file
}

#
# ddrsync_human_amount
#

# bats test_tags=ddrsync_human_amount:0

@test "ddrsync_human_amount argument test good with no argument (rcode 0)" {
  run -0 ddrsync_human_amount
}

# bats test_tags=ddrsync_human_amount:0

@test "ddrsync_human_amount argument test good with integer less than 1T (rcode 0)" {
  run -0 ddrsync_human_amount 100
}

# bats test_tags=ddrsync_human_amount:0

@test "ddrsync_human_amount argument test good with float less than 1.0T (rcode 0)" {
  run -0 ddrsync_human_amount 100.00
}

# bats test_tags=ddrsync_human_amount:1

@test "ddrsync_human_amount argument test argument not a number  (rcode 1)" {
  run -1 ddrsync_human_amount ONE
}




