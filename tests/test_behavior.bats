
bats_require_minimum_version 1.5.0

# Load the tested functions and the default configuration
load ${BATS_TEST_DIRNAME}/../func/f_ddrsync.sh
load ${BATS_TEST_DIRNAME}/../etc/ddrsync.conf

#
# Global test setup
#

setup_file() {

  # Prepare the source directory
  export DDRSYNC_SRC_DIR=${BATS_SUITE_TMPDIR}/src
  mkdir ${DDRSYNC_SRC_DIR}
  dd if=/dev/urandom of=${DDRSYNC_SRC_DIR}/existent-file bs=512 count=1
  dd if=/dev/urandom of=${DDRSYNC_SRC_DIR}/.dot-file bs=512 count=1
  # create a FIFO file for nonregular file tests
  mknod ${DDRSYNC_SRC_DIR}/nonregular-file p
  touch ${DDRSYNC_SRC_DIR}/empty-file
  ln -s ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/symlink
  mkdir ${DDRSYNC_SRC_DIR}/existent-dir
  # build a directory tree
  mkdir ${DDRSYNC_SRC_DIR}/tree
  cp ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/tree
  cp ${DDRSYNC_SRC_DIR}/.dot-file ${DDRSYNC_SRC_DIR}/tree
  mkdir ${DDRSYNC_SRC_DIR}/tree/dir1
  cp ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/tree/dir1
  cp ${DDRSYNC_SRC_DIR}/.dot-file ${DDRSYNC_SRC_DIR}/tree/dir1
  mkdir ${DDRSYNC_SRC_DIR}/tree/dir2
  cp ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/tree/dir2
  cp ${DDRSYNC_SRC_DIR}/tree/.dot-file ${DDRSYNC_SRC_DIR}/tree/dir2
  mkdir ${DDRSYNC_SRC_DIR}/tree/dir1/dir1.1
  cp ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/tree/dir1/dir1.1
  cp ${DDRSYNC_SRC_DIR}/.dot-file ${DDRSYNC_SRC_DIR}/tree/dir1/dir1.1
  # we keep dir1.2 empty
  mkdir ${DDRSYNC_SRC_DIR}/tree/dir1/dir1.2
  mkdir ${DDRSYNC_SRC_DIR}/tree/.dot-dir
  cp ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/tree/.dot-dir
  cp ${DDRSYNC_SRC_DIR}/.dot-file ${DDRSYNC_SRC_DIR}/tree/.dot-dir

  # Create the destination directory
  export DDRSYNC_DST_DIR=${BATS_SUITE_TMPDIR}/dst
  mkdir ${DDRSYNC_DST_DIR}
  touch ${DDRSYNC_DST_DIR}/nondirectory-entry

}

# setup of each test
setup() {
  # echo "setup" >&3
  # ls -al ${DDRSYNC_DST_DIR} >&3
  dd if=/dev/urandom of=${DDRSYNC_SRC_DIR}/random-file bs=512 count=1
  dd if=/dev/urandom of=${DDRSYNC_DST_DIR}/different-random-file bs=256 count=1
}

teardown() {
  # echo "teardown" >&3
  # Remove source random-file for next test to regenerate a different one
  rm -f ${DDRSYNC_SRC_DIR}/random-file

  # Remove potential rsynced files in destination to be clean for next test
  rm -f ${DDRSYNC_DST_DIR}/.dot-file
  rm -f ${DDRSYNC_DST_DIR}/empty-file
  rm -f ${DDRSYNC_DST_DIR}/random-file
  rm -f ${DDRSYNC_DST_DIR}/different-random-file  
  rm -rf ${DDRSYNC_DST_DIR}/existent-dir
  rm -rf ${DDRSYNC_DST_DIR}/tree
}

# bats file_tags=behaviour

#
# ddrsync_init
#

# bats test_tags=ddrsync_init:0

@test "ddrsync_init check (rcode 0)" {
  ddrsync_init
  [ ${DDRSYNC_DATA_TOTAL} -eq 0 ]
}

#
# ddrsync_block_size
#

# bats test_tags=ddrsync_block_size:0

@test "ddrsync_block_size check without argument (rcode 0)" {
  run -0 ddrsync_block_size
  [ "${output}" = "${DDRSYNC_BLOCK_SIZE}" ]
}

# bats test_tags=ddrsync_block_size:1

@test "ddrsync_block_size check with argument (rcode 0)" {
  run -0 ddrsync_block_size 32M
  ddrsync_block_size 32M
  [ "${DDRSYNC_BLOCK_SIZE}" = "32M" ]
  run -0 ddrsync_block_size
  [ "${output}" = "32M" ]
}

#
# ddrsync_verbose
#

# bats test_tags=ddrsync_block_size:0

@test "ddrsync_verbose check without argument (rcode 0)" {
  run -0 ddrsync_verbose
  [ "${output}" = "${DDRSYNC_VERBOSE}" ]
}

# bats test_tags=ddrsync_block_size:1

@test "ddrsync_verbose check with argument (rcode 0)" {
  run -0 ddrsync_verbose 1
  ddrsync_verbose 1
  [ "${DDRSYNC_VERBOSE}" = "1" ]
  run -0 ddrsync_verbose
  [ "${output}" = "1" ]
}


#
# ddrsync_get_entry_type
#

# bats test_tags=ddrsync_get_entry_type:0

@test "ddrsync_get_entry_type argument test regular file (rcode 0)" {
  run -0 ddrsync_get_entry_type ${DDRSYNC_SRC_DIR}/existent-file
  [ "${output}" -eq "${DDRSYNC_ENTRY_REGULAR_FILE}" ]
  run -0 ddrsync_get_entry_type ${DDRSYNC_SRC_DIR}/.dot-file
  [ "${output}" -eq "${DDRSYNC_ENTRY_REGULAR_FILE}" ]
  run -0 ddrsync_get_entry_type ${DDRSYNC_SRC_DIR}/empty-file
  [ "${output}" -eq "${DDRSYNC_ENTRY_REGULAR_FILE}" ]
}

# bats test_tags=ddrsync_get_entry_type:1

@test "ddrsync_get_entry_type argument test symlink (rcode 0)" {
  run -0 ddrsync_get_entry_type ${DDRSYNC_SRC_DIR}/symlink
  [ "${output}" -eq "${DDRSYNC_ENTRY_SYMLINK}" ]
}

# bats test_tags=ddrsync_get_entry_type:2

@test "ddrsync_get_entry_type argument test directory (rcode 0)" {
  run -0 ddrsync_get_entry_type ./
  [ "${output}" -eq "${DDRSYNC_ENTRY_DIRECTORY}" ]
  run -0 ddrsync_get_entry_type ${DDRSYNC_SRC_DIR}/..
  [ "${output}" -eq "${DDRSYNC_ENTRY_DIRECTORY}" ]
  run -0 ddrsync_get_entry_type ${DDRSYNC_SRC_DIR}/existent-dir
  [ "${output}" -eq "${DDRSYNC_ENTRY_DIRECTORY}" ]
}

# bats test_tags=ddrsync_get_entry_type:3

@test "ddrsync_get_entry_type argument test non regular file (rcode 0)" {
  run -0 ddrsync_get_entry_type ${DDRSYNC_SRC_DIR}/nonregular-file
  [ "${output}" -eq "${DDRSYNC_ENTRY_OTHER}" ]
}

#
# ddrsync_cp_file
#

# bats test_tags=ddrsync_cp_file:0

@test "ddrsync_cp_file test a copy and rename of a file from source to destination (rcode 0)" {
  # Create a copy in destination with a new name
  run -0 ddrsync_cp_file ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}/copied-file
  run -0 diff -b ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}/copied-file
}

# bats test_tags=ddrsync_cp_file:1

@test "ddrsync_cp_file test a copy of a file from source to destination directory (rcode 0)" {
  # Copy the random-file in destination directory
  run -0 ddrsync_cp_file ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}
  run -0 diff -b ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}/random-file
}

# bats test_tags=ddrsync_cp_file:2

@test "ddrsync_cp_file test a copy of a source file that overwrites a destination file (rcode 0)" {
  # Overwrite different-random-file in destination directory
  run -1 diff -b ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}/different-random-file
  run -0 ddrsync_cp_file ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}/different-random-file
  run -0 diff -b ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}/different-random-file
}

#
# ddrsync_rsync_file
#

# bats test_tags=ddrsync_rsync_file:0

@test "ddrsync_rsync_file compare a ddrsync with a rsync and rename of a file from source to destination (rcode 0)" {
  run -0 ddrsync_rsync_file ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}/rsynced-random-file
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}/rsynced-random-file
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

# bats test_tags=ddrsync_rsync_file:1

@test "ddrsync_rsync_file compare a ddrsync with a rsync of a file from source to destination directory (rcode 0)" {
  run -0 ddrsync_rsync_file ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

# bats test_tags=ddrsync_rsync_file:2

@test "ddrsync_rsync_file compare a ddrsync with a rsync of a EMPTY file from source to destination directory (rcode 0)" {
  run -0 ddrsync_rsync_file ${DDRSYNC_SRC_DIR}/empty-file ${DDRSYNC_DST_DIR}
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/empty-file ${DDRSYNC_DST_DIR}
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

# bats test_tags=ddrsync_rsync_file:3

@test "ddrsync_rsync_file compare a ddrsync with a rsync of a DOT file from source to destination directory (rcode 0)" {
  run -0 ddrsync_rsync_file ${DDRSYNC_SRC_DIR}/.dot-file ${DDRSYNC_DST_DIR}
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/.dot-file ${DDRSYNC_DST_DIR}
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

# bats test_tags=ddrsync_rsync_file:4

@test "ddrsync_rsync_file test a ddrsync of a file already rsynced to destination directory (rcode 0)" {
  # First we rsync the file
  run -0 rsync -a ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}
  # Then we ddrsync the same file, nothing should be done
  export DDRSYNC_VERBOSE=1
  run -0 ddrsync_rsync_file ${DDRSYNC_SRC_DIR}/random-file ${DDRSYNC_DST_DIR}
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

#
# ddrsync_rsync_symlink
#

# bats test_tags=ddrsync_rsync_symlink:0

@test "ddrsync_rsync_symlink test rsync of a symlink from source to destination (rcode 0)" {
  skip "ddrsync_rsync_symlink not implemented"
  run -0 ddrsync_rsync_symlink ${DDRSYNC_SRC_DIR}/ ${DDRSYNC_DST_DIR}/copied-file
}

#
# ddrsync_rsync_directory
#

# bats test_tags=ddrsync_rsync_directory:0

@test "ddrsync_rsync_directory compare a ddrsync with rsync for copying and renaming a directory (rcode 0)" {
  run -0 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/existent-dir ${DDRSYNC_DST_DIR}/new-dir
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/existent-dir/ ${DDRSYNC_DST_DIR}/new-dir
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

# bats test_tags=ddrsync_rsync_directory:1

@test "ddrsync_rsync_directory compare a ddrsync with rsync for creating same directory into destination (rcode 0)" {
  run -0 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/existent-dir ${DDRSYNC_DST_DIR}
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/existent-dir/ ${DDRSYNC_DST_DIR}
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

# bats test_tags=ddrsync_rsync_directory:2

@test "ddrsync_rsync_directory update date of an older directory in destination with ddrsync (rcode 0)" {
  mkdir ${DDRSYNC_DST_DIR}/existent-dir
  touch -d "$(date -d '-1 hour')" ${DDRSYNC_SRC_DIR}/existent-dir
  run -0 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/existent-dir ${DDRSYNC_DST_DIR}/existent-dir 
  [ $(stat -c "%Y" ${DDRSYNC_SRC_DIR}/existent-dir) -eq $(stat -c "%Y" ${DDRSYNC_DST_DIR}/existent-dir) ]
}

# bats test_tags=ddrsync_rsync_directory:3

@test "ddrsync_rsync_directory update date of a more recent directory in destination with ddrsync (rcode 0)" {
  mkdir ${DDRSYNC_DST_DIR}/existent-dir
  touch -d "$(date -d '+1 hour')" ${DDRSYNC_SRC_DIR}/existent-dir
  run -0 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/existent-dir ${DDRSYNC_DST_DIR}/existent-dir
  [ $(stat -c "%Y" ${DDRSYNC_SRC_DIR}/existent-dir) -eq $(stat -c "%Y" ${DDRSYNC_DST_DIR}/existent-dir) ]
}

# bats test_tags=ddrsync_rsync_directory:4

@test "ddrsync_rsync_directory ddrsync a tree with files and sub-directories and compare with rsync (rcode 0)" {
  run -0 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/tree ${DDRSYNC_DST_DIR}/tree
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/tree/ ${DDRSYNC_DST_DIR}/tree
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

# bats test_tags=ddrsync_rsync_directory:5

@test "ddrsync_rsync_directory rsync a tree with files and sub-directories and compare with ddrsync (rcode 0)" {
  run -0 rsync -ai ${DDRSYNC_SRC_DIR}/tree/ ${DDRSYNC_DST_DIR}/tree
  export DDRSYNC_VERBOSE=1
  run -0 ddrsync_rsync_directory ${DDRSYNC_SRC_DIR}/tree ${DDRSYNC_DST_DIR}/tree
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

#
# ddrsync_rsync_selector
#

# bats test_tags=ddrsync_rsync_selector:0

@test "ddrsync_rsync_selector ddrsync a file and compare with rsync (rcode 0)" {
  run -0 ddrsync_rsync_selector ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_DST_DIR}
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_DST_DIR}/existent-file
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

# bats test_tags=ddrsync_rsync_selector:1

@test "ddrsync_rsync_selector ddrsync a symlink and compare with rsync (rcode 0)" {
  skip "ddrsync_rsync_symlink not implemented"  
  run -0 ddrsync_rsync_selector ${DDRSYNC_SRC_DIR}/symlink ${DDRSYNC_DST_DIR}
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/symlink ${DDRSYNC_DST_DIR}/symlink
  # echo "$output" >&3
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

# bats test_tags=ddrsync_rsync_selector:2

@test "ddrsync_rsync_selector ddrsync a tree with sub-directories and files compare with rsync (rcode 0)" {
  run -0 ddrsync_rsync_selector ${DDRSYNC_SRC_DIR}/tree ${DDRSYNC_DST_DIR}/tree
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/tree/ ${DDRSYNC_DST_DIR}/tree
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}

#
# ddrsync_human_amount
#

# bats test_tags=ddrsync_human_amount:0

@test "ddrsync_human_amount test a bytes value (rcode 0)" {
  run -0 ddrsync_human_amount 100
  [ "${output}" = "100.00" ]
}

# bats test_tags=ddrsync_human_amount:1

@test "ddrsync_human_amount test a Kbytes value (rcode 0)" {
  run -0 ddrsync_human_amount 100000
  [ "${output}" = "100.00K" ]
}

# bats test_tags=ddrsync_human_amount:2

@test "ddrsync_human_amount test a Mbytes value (rcode 0)" {
  run -0 ddrsync_human_amount 100000000
  [ "${output}" = "100.00M" ]
}

# bats test_tags=ddrsync_human_amount:3

@test "ddrsync_human_amount test a Gbytes value (rcode 0)" {
  run -0 ddrsync_human_amount 100000000000
  [ "${output}" = "100.00G" ]
}

# bats test_tags=ddrsync_human_amount:4

@test "ddrsync_human_amount test a Tbytes value but output in Gbytes (rcode 0)" {
  run -0 ddrsync_human_amount 100000000000000
  [ "${output}" = "100000.00G" ]
}

