
bats_require_minimum_version 1.5.0

#
# Global test setup
#

setup_file() {

  # Make the commands executable and available from PATH
  chmod 755 ${BATS_TEST_DIRNAME}/../bin/ddrsync
  export PATH=${BATS_TEST_DIRNAME}/../bin:${PATH}

  # Prepare the source directory
  export DDRSYNC_SRC_DIR=${BATS_SUITE_TMPDIR}/src
  mkdir ${DDRSYNC_SRC_DIR}
  dd if=/dev/urandom of=${DDRSYNC_SRC_DIR}/existent-file bs=512 count=1
  dd if=/dev/urandom of=${DDRSYNC_SRC_DIR}/.dot-file bs=512 count=1
  # create a FIFO file for nonregular file tests
  mknod ${DDRSYNC_SRC_DIR}/nonregular-file p
  touch ${DDRSYNC_SRC_DIR}/empty-file
  ln -s ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/symlink
  mkdir ${DDRSYNC_SRC_DIR}/existent-dir
  # build a directory tree
  mkdir ${DDRSYNC_SRC_DIR}/tree
  cp ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/tree
  cp ${DDRSYNC_SRC_DIR}/.dot-file ${DDRSYNC_SRC_DIR}/tree
  mkdir ${DDRSYNC_SRC_DIR}/tree/dir1
  cp ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/tree/dir1
  cp ${DDRSYNC_SRC_DIR}/.dot-file ${DDRSYNC_SRC_DIR}/tree/dir1
  mkdir ${DDRSYNC_SRC_DIR}/tree/dir2
  cp ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/tree/dir2
  cp ${DDRSYNC_SRC_DIR}/tree/.dot-file ${DDRSYNC_SRC_DIR}/tree/dir2
  mkdir ${DDRSYNC_SRC_DIR}/tree/dir1/dir1.1
  cp ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/tree/dir1/dir1.1
  cp ${DDRSYNC_SRC_DIR}/.dot-file ${DDRSYNC_SRC_DIR}/tree/dir1/dir1.1
  # we keep dir1.2 empty
  mkdir ${DDRSYNC_SRC_DIR}/tree/dir1/dir1.2
  mkdir ${DDRSYNC_SRC_DIR}/tree/.dot-dir
  cp ${DDRSYNC_SRC_DIR}/existent-file ${DDRSYNC_SRC_DIR}/tree/.dot-dir
  cp ${DDRSYNC_SRC_DIR}/.dot-file ${DDRSYNC_SRC_DIR}/tree/.dot-dir

  # Create the destination directory
  export DDRSYNC_DST_DIR=${BATS_SUITE_TMPDIR}/dst
  mkdir ${DDRSYNC_DST_DIR}
  touch ${DDRSYNC_DST_DIR}/nondirectory-entry

}

# setup of each test
setup() {
  # echo "setup" >&3
  # ls -al ${DDRSYNC_DST_DIR} >&3
  dd if=/dev/urandom of=${DDRSYNC_SRC_DIR}/random-file bs=512 count=1
  dd if=/dev/urandom of=${DDRSYNC_DST_DIR}/different-random-file bs=256 count=1
}

teardown() {
  # echo "teardown" >&3
  # Remove source random-file for next test to regenerate a different one
  rm -f ${DDRSYNC_SRC_DIR}/random-file

  # Remove potential rsynced files in destination to be clean for next test
  rm -f ${DDRSYNC_DST_DIR}/.dot-file
  rm -f ${DDRSYNC_DST_DIR}/empty-file
  rm -f ${DDRSYNC_DST_DIR}/random-file
  rm -f ${DDRSYNC_DST_DIR}/different-random-file  
  rm -rf ${DDRSYNC_DST_DIR}/existent-dir
  rm -rf ${DDRSYNC_DST_DIR}/tree
}

# bats file_tags=behaviour

#
# ddrsync
#

# bats test_tags=ddrsync:0

# bats test_tags=ddrsync:1

@test "ddrsync test usage (-h option)  (rcode 0)" {
  run -0 ddrsync -h
  echo "${output}" | grep "Usages:" &>/dev/null
}

# bats test_tags=ddrsync:2

@test "ddrsync test block size option with usage  (rcode 0)" {
  run -0 ddrsync -h -b 256M
  echo "${output}" | grep "Current value is : 256M" &>/dev/null
}

# bats test_tags=ddrsync:3

@test "ddrsync test missing arguments  (rcode 0)" {
  run -1 ddrsync
  run -1 ddrsync src
}

# bats test_tags=ddrsync:4

@test "ddrsync test rsync of a directory tree (rcode 0)" {
  run -0 ddrsync ${DDRSYNC_SRC_DIR}/tree ${DDRSYNC_DST_DIR}/tree
  run -0 rsync -ani ${DDRSYNC_SRC_DIR}/tree/ ${DDRSYNC_DST_DIR}/tree
  # Count the non-blank lines of rsync output. Should be none.
  [ $(echo "${output}" | sed '/^\s*$/d' | wc -l) -eq 0 ]
}
  

